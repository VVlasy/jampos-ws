#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include "httpPacket.h"

char *HttpMethodsStrings[] = {
    "",
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "CONNECT",
    "OPTIONS",
    "TRACE"};

THTTPSTATUS HttpStatusValues[] = {
    // Information
    {100, "Continue"},
    {101, "Switching Protocols"},

    // Successful
    {200, "OK"},
    {201, "Created"},
    {202, "Accepted"},
    {203, "Non-authoritative Information"},
    {204, "No Content"},
    {205, "Reset Content"},
    {206, "Partial Content"},

    // Redirection
    {300, "Multiple Choices"},
    {301, "Moved Permanently"},
    {302, "Found"},
    {303, "See Other"},
    {304, "Not Modified"},
    {305, "Use Proxy"},
    {306, "Unused"},
    {307, "Temporary Redirect"},

    // Client error
    {400, "Bad Request"},
    {401, "Unauthorized"},
    {402, "Payment Required"},
    {403, "Forbidden"},
    {404, "Not Found"},
    {405, "Method Not Allowed"},
    {406, "Not Acceptable"},
    {407, "Proxy Authentication Required"},
    {408, "Request Timeout"},
    {409, "Conflict"},
    {410, "Gone"},
    {411, "Length Required"},
    {412, "Precondition Failed"},
    {413, "Request Entity Too Large"},
    {414, "Request-url Too Long"},
    {415, "Unsupported Media Type"},
    {416, "Requested Range Not Satisfiable"},
    {417, "Expectation Failed"},
    {418, "I'm a teapot"},

    // Server error
    {500, "Internal Server Error"},
    {501, "Not Implemented"},
    {502, "Bad Gateway"},
    {503, "Service Unavailable"},
    {504, "Gateway Timeout"},
    {505, "HTTP Version Not Supported"},
};

THTTPRESPONSEPACKET deserializeRequest(char *data, THTTPREQUESTPACKET *deserialized)
{
    assert(deserialized != NULL);
    assert(data != NULL);

    int bufferRead = 0;
    int headerLen = 0;

    char tmp;

    enum HttpStatusCodes statusCode = OK;
    THTTPRESPONSEPACKET result = {
        .status = HttpStatusValues[statusCode],
        .httpVersion = 1.1, // TODO: Get from internal engine max supported, drop down if client lower
    };

    char *method = (char *)malloc(sizeof(char) * 8);

    if (sscanf(data, "%7s %4096s HTTP/%lf %c%n", method, deserialized->resource, &deserialized->httpVersion, &tmp, &bufferRead) != 4 || (deserialized->method = getHttpMethodFromString(method)) == MethodFail)
    {
        statusCode = BadRequest;
        result.status = HttpStatusValues[statusCode];
        free(method);
        return result;
    }
    free(method);
    data += bufferRead - 1;

    if (deserialized->httpVersion > 1.1)
    { // TODO: Get max supported from engine
        statusCode = BadRequest;
        result.status = HttpStatusValues[statusCode];
        return result;
    }

    // Read headers
    do
    {
        THTTPHEADER *header = (THTTPHEADER *)calloc(1, sizeof(THTTPHEADER));
        headerLen = strchr(data, '\n') - data;

        if (!readHeader(data, headerLen, header))
        {
            free(header);
            break;
        }

        data += headerLen + 1;

        deserialized->headers = (THTTPHEADER **)realloc(deserialized->headers, sizeof(THTTPHEADER *) * ++deserialized->headerCount);
        deserialized->headers[deserialized->headerCount - 1] = header;
    } while (headerLen != 0);

    return result;
}

enum HttpMethods getHttpMethodFromString(char *methodString)
{
    assert(methodString != NULL);

    for (unsigned int i = 1; i < sizeof(HttpMethodsStrings); i++)
    {
        if (!strcmp(methodString, HttpMethodsStrings[i]))
        {
            return (enum HttpMethods)i;
        }
    }

    return MethodFail;
}

void addHeader(THTTPHEADER ***headers, int *count, const char *name, const char *value)
{
    THTTPHEADER *h = (THTTPHEADER *)malloc(sizeof(THTTPHEADER));
    h->name = (char *)malloc(strlen(name) + 1);
    h->value = (char *)malloc(strlen(value) + 1);
    strcpy(h->name, name);
    strcpy(h->value, value);

    (*count)++;
    (*headers) = (THTTPHEADER **)realloc(*headers, sizeof(THTTPHEADER *) * (*count));
    (*headers)[(*count) - 1] = h;
}

int readHeader(char *buff, int len, THTTPHEADER *h)
{
    char *test = strchr(buff, ':');

    if (test == 0x0)
    {
        return 0;
    }

    int valueNameSep = test - buff;
    h->name = (char *)calloc(valueNameSep + 1, sizeof(char));
    memcpy(h->name, buff, valueNameSep);

    buff += valueNameSep;

    while (buff[len - valueNameSep] == ' ' || buff[len - valueNameSep] == '\n' || buff[len - valueNameSep] == '\r')
        valueNameSep++;

    do
    {
        buff++;
    } while (*buff == ' ');

    h->value = (char *)calloc(len - valueNameSep, sizeof(char));
    memcpy(h->value, buff, len - valueNameSep - 1);

    return 1;
}

int getHeadersLen(THTTPHEADER **headers, int count)
{
    int charSize = 0;

    for (int i = 0; i < count; i++)
    {
        charSize += strlen(headers[i]->name) + 2 + strlen(headers[i]->value) + 2;
    }

    return charSize;
}

void printHeaders(char *buffer, THTTPHEADER **headers, int count)
{
    int skip = 0;
    for (int i = 0; i < count; i++)
    {
        sprintf(buffer + skip, "%s: %s\r\n", headers[i]->name, headers[i]->value);
        skip += snprintf(NULL, 0, "%s: %s\r\n", headers[i]->name, headers[i]->value);
    }
}

char *serializeResponse(THTTPRESPONSEPACKET p, int *size)
{
    int responseSize = snprintf(NULL, 0, "HTTP/%.1lf %d %s\r\n", p.httpVersion, p.status.code, p.status.text);
    int bodySize = p.body != 0x0 ? p.bodySize : 0;

    responseSize += getHeadersLen(p.headers, p.headerCount);
    responseSize += 2;
    responseSize += bodySize;

    char *data = (char *)calloc(responseSize, sizeof(char));

    sprintf(data, "HTTP/%.1lf %d %s\r\n", p.httpVersion, p.status.code, p.status.text);
    printHeaders(data + strlen(data), p.headers, p.headerCount);
    sprintf(data + strlen(data), "\r\n");

    if(p.body != 0x0)
        memcpy(data + strlen(data), p.body, bodySize);

    (*size) = responseSize;        
    return data;
}

void writeDefaultResponseHeaders(THTTPRESPONSEPACKET *packet)
{
    char dateStr[1000];
    time_t now = time(0);
    struct tm tm = *gmtime(&now);
    strftime(dateStr, sizeof(dateStr), "%a, %d %b %Y %H:%M:%S %Z", &tm);
    addHeader(&packet->headers, &packet->headerCount,
              "Date", dateStr);

    addHeader(&packet->headers, &packet->headerCount,
              "Server", "jampos");
    addHeader(&packet->headers, &packet->headerCount,
              "Last-Modified", "Mon, 29 Jul 2009 12:28:53 GMT"); // TODO
    addHeader(&packet->headers, &packet->headerCount,
              "ETag", "34aa387-d-1568eb00");
    addHeader(&packet->headers, &packet->headerCount,
              "Vary", "Authorization,Accept");
    addHeader(&packet->headers, &packet->headerCount,
              "Accept-Ranges", "bytes");

    if(packet->bodySize > 0){
        char *conLen = (char *)calloc(16, sizeof(char));
        sprintf(conLen, "%d", packet->bodySize);
        addHeader(&packet->headers, &packet->headerCount,
              "Content-Length", conLen);
        free(conLen);
    }
    

    addHeader(&packet->headers, &packet->headerCount,
              "Connection", "close");
}

void freeReqPacket(THTTPREQUESTPACKET *packet){
    free(packet->resource);
    for(int i = 0; i < packet->headerCount; i++){
        free(packet->headers[i]->name);
        free(packet->headers[i]->value);
        free(packet->headers[i]);
    }
    free(packet->headers);
    
    if(packet->body)
        free(packet->body);
    free(packet);
}

void freeResPacket(THTTPRESPONSEPACKET *packet){
    for(int i = 0; i < packet->headerCount; i++){
        free(packet->headers[i]->name);
        free(packet->headers[i]->value);
        free(packet->headers[i]);
    }
    free(packet->headers);
    
    if(packet->body)
        free(packet->body);
    free(packet);
}