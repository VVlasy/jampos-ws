#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>

#include "httpPacket.h"
#include "server.h"
#include "userMenu.h"
#include "fileFinder.h"

#define PORT 8080
#define RECVBUFFERINCREMENT 1024
#define BODYBUFFERINCREMENT 1024
#define MAXURILENGTH 4096

pthread_t serverThreadId = NULL;

void startServer()
{
    if (serverThreadId == NULL)
    {
        printToUser("Server started.\n");

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

        pthread_create(&serverThreadId, &attr, serverStartProc, NULL);
    }
}

void stopServer()
{
    if (serverThreadId != NULL)
    {
        printToUser("Stopping server.\n");
        pthread_cancel(serverThreadId);
        serverThreadId = NULL;
    }
}

void *serverStartProc(void *vargp)
{
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    int threadCount = 0;
    pthread_t *connectionThreads = NULL;

    pthread_cleanup_push(serverCleanupProc, &server_fd);

    // Creating socket file descriptor
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Forcefully attaching socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Forcefully attaching socket to the port 8080
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                                (socklen_t *)&addrlen)) >= 0)
    {
        TCONNECTIONDATA *connData =
            (TCONNECTIONDATA *)malloc(sizeof(TCONNECTIONDATA));

        connData->socketId = new_socket;

        // TODO: go through threads and cleanup finished ones
        threadCount++;
        connectionThreads = (pthread_t *)realloc(connectionThreads,
                                                 sizeof(pthread_t) * threadCount);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

        pthread_create(&connectionThreads[threadCount - 1], &attr,
                       connectionHandler, connData);
    }

    pthread_cleanup_pop(1);

    return 0;
}

void serverCleanupProc(void *arg)
{
    int socketFd = *(int *)arg;

    close(socketFd);
}

void *connectionHandler(void *vargp)
{
    TCONNECTIONDATA *connD = (TCONNECTIONDATA *)vargp;
    THTTPREQUESTPACKET *reqPacket =
        (THTTPREQUESTPACKET *)calloc(1, sizeof(THTTPREQUESTPACKET));
    THTTPRESPONSEPACKET *defaultResponse =
        (THTTPRESPONSEPACKET *)calloc(1, sizeof(THTTPREQUESTPACKET));

    reqPacket->resource = (char *)calloc(sizeof(char), (MAXURILENGTH + 1));

    int readResult = 0, writePos = 0;

    int bufferSize = 0;
    char *buffer = NULL;

    char *response;
    char *pathToResource = NULL;

    do
    {
        writePos += readResult;
        bufferSize += RECVBUFFERINCREMENT;
        buffer = (char *)realloc(buffer, bufferSize * sizeof(char));

        for (int i = bufferSize - RECVBUFFERINCREMENT; i < bufferSize; i++)
        {
            buffer[i] = '\0';
        }
    } while ((readResult = read(connD->socketId, buffer + writePos,
                                RECVBUFFERINCREMENT)) == 1024);
    writePos += readResult;

    if(!writePos){
        // empty packet or not big enough packet
        free(reqPacket->resource);
        free(reqPacket);

        free(defaultResponse);

        free(buffer);
        close(connD->socketId);
        free(connD);
        return;
    }

    printToUser(buffer);

    (*defaultResponse) = deserializeRequest(buffer, reqPacket);
    free(buffer);

    // Do the job
    FILE *f = getFileByResourceName(reqPacket->resource, &pathToResource);

    if (f)
    {
        int c = '\0';

        int pos = 0;
        int size = BODYBUFFERINCREMENT;
        defaultResponse->body = (char *)malloc(BODYBUFFERINCREMENT * sizeof(char));

        while ((c = fgetc(f)) != EOF)
        {
            if (pos == size - 1)
            {
                size += BODYBUFFERINCREMENT;
                defaultResponse->body =
                    (char *)realloc(defaultResponse->body, size * sizeof(char));
                
                memset(defaultResponse->body + size - BODYBUFFERINCREMENT, '\0', BODYBUFFERINCREMENT);
            }
            defaultResponse->body[pos] = c;
            pos++;
        }

        defaultResponse->body[pos] = '\0';
        defaultResponse->bodySize = pos + 1;

        fclose(f);

        char *filetype = getHttpFileTypeString(pathToResource);
        addHeader(&defaultResponse->headers, &defaultResponse->headerCount,
            "Content-Type", filetype);
        free(filetype);
    }
    else
    {
        defaultResponse->status = HttpStatusValues[NotFound];

        // TODO: get error body !MAYBE!
        defaultResponse->body = (char *)malloc(strlen("Not Found." + 1));
        strcpy(defaultResponse->body, "Not found.");
        defaultResponse->bodySize = strlen(defaultResponse->body);

        addHeader(&defaultResponse->headers, &defaultResponse->headerCount,
            "Content-Type", "text/html");
    }
    
    writeDefaultResponseHeaders(defaultResponse);
    
    int responseSize = 0;
    response = serializeResponse(*defaultResponse, &responseSize);
    
    send(connD->socketId, response, responseSize, 0);
    printToUser("Response sent\n");

    // The end
    freeReqPacket(reqPacket);
    freeResPacket(defaultResponse);
    if(pathToResource)
        free(pathToResource);
    free(response);

    close(connD->socketId);
    free(connD);
    return 0;
}