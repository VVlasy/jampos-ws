#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fileFinder.h"

#define SRCFOLDER "./doc"
#define DEFAULTINDEX "index.html"

// TODO: splitout
char *strdup(const char *s) {
  size_t slen = strlen(s);
  char *result = (char *)malloc(slen + 1);
  if (result == NULL) {
    return NULL;
  }

  memcpy(result, s, slen + 1);
  return result;
}

FILE *getFileByResourceName(const char *resource, char **out) {
  int pathSize = strlen(SRCFOLDER) + strlen(resource) + 1;
  char *path = (char *)malloc(sizeof(char) * pathSize);
  FILE *f;

  // Copy base folder
  strcpy(path, SRCFOLDER);
  strcpy(path + strlen(SRCFOLDER), resource);

  if (path[pathSize - 2] == '/') {
    pathSize += strlen(DEFAULTINDEX);
    path = (char *)realloc(path, sizeof(char) * pathSize);
    strcpy(path + (pathSize - strlen(DEFAULTINDEX) - 1), DEFAULTINDEX);
  }

  if (access(path, F_OK) == -1) {
    free(path);
    return NULL;
  }

  f = fopen(path, "r");
  (*out) = path;

  return f;
}

char *getHttpFileTypeString(const char *path) {
  char *fileName = strrchr(path, '/');
  char *dot = strrchr(fileName, '.');

  if (strcmp(dot + 1, "html") == 0) {
    return strdup("text/html");
  } else if (strcmp(dot + 1, "php") == 0) {
    return strdup("text/html");
  } else if (strcmp(dot + 1, "js") == 0) {
    return strdup("text/javascript");
  } else if (strcmp(dot + 1, "css") == 0) {
    return strdup("text/css");
  } else if (strcmp(dot + 1, "xml") == 0) {
    return strdup("text/xml");
  } else if (strcmp(dot + 1, "xhtml") == 0) {
    return strdup("text/xhtml");
  }

  if (strcmp(dot + 1, "jpg") == 0 || strcmp(dot + 1, "jpeg") == 0) {
    return strdup("image/jpeg");
  } else if (strcmp(dot + 1, "png") == 0) {
    return strdup("image/png");
  } else if (strcmp(dot + 1, "bmp") == 0) {
    return strdup("image/bmp");
  } else if (strcmp(dot + 1, "ico") == 0) {
    return strdup("image/x-icon");
  }

  if (strcmp(dot + 1, "ogg") == 0) {
    return strdup("audio/ogg");
  } else if (strcmp(dot + 1, "mpeg") == 0) { // todo: audio
    return strdup("audio/mpeg");
  } else if (strcmp(dot + 1, "mp3") == 0) {
    return strdup("audio/mp3");
  } else if (strcmp(dot + 1, "wav") == 0) {
    return strdup("audio/wav");
  } else if (strcmp(dot + 1, "wma") == 0) {
    return strdup("audio/wma");
  } else if (strcmp(dot + 1, "flac") == 0) {
    return strdup("audio/flac");
  }

  if (strcmp(dot + 1, "mkv") == 0) {
    return strdup("video/mkv");
  } else if (strcmp(dot + 1, "webm") == 0) {
    return strdup("video/webm");
  } else if (strcmp(dot + 1, "flv") == 0) {
    return strdup("video/flv");
  } else if (strcmp(dot + 1, "vob") == 0) {
    return strdup("video/vob");
  } else if (strcmp(dot + 1, "ogv") == 0) {
    return strdup("video/ogv");
  } else if (strcmp(dot + 1, "avi") == 0) {
    return strdup("video/avi");
  } else if (strcmp(dot + 1, "mov") == 0) {
    return strdup("video/mov");
  } else if (strcmp(dot + 1, "mp4") == 0) {
    return strdup("video/mp4");
  }

  // Otherwise consider it application
  //TODO:
  return strdup("application/unknown");
}