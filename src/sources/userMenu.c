#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "userMenu.h"
#include "server.h"

void printToUser(char *text){
    printf("%s\n", text);
}

void printMenu(){
    printf("Welcome to Just Another MultiPlatform Open Source Web Server\n\n");

    printf("Choose a menu option by pressing the corresponding letter:\n");
    printf("s) start web server\n");
    printf("e) stop web server\n");
    printf("q) close application\n");

    printf("\nApplication output:\n");
}

void startWithMenu(){
    int runnning = 1;

    printMenu();

    while(runnning){
        switch(getchar()){
            case 's':
                startServer();
                break;
            case 'e':
                stopServer();
                break;
            case 'q':
                exit(0);
                break;
            default:
                break;
        }

        while(getchar()!='\n');
    }
}