#ifndef HTTPPACKET_H_INCLUDED
#define HTTPPACKET_H_INCLUDED

enum HttpMethods
{
    MethodFail = 0,
    Get = 1,
    Head = 2,
    Post = 3,
    Put = 4,
    Delete = 5,
    Connect = 6,
    Options = 7,
    Trace = 8
};

enum HttpStatusCodes
{
    Continue = 0,
    SwitchingProtocols = 1,
    OK = 2,
    Created = 3,
    Accepted = 4,
    NonAuthoritativeInformation = 5,
    NoContent = 6,
    ResetContent = 7,
    PartialContent = 8,
    MultipleChoices = 9,
    MovedPermanently = 10,
    Found = 11,
    SeeOther = 12,
    NotModified = 13,
    UseProxy = 14,
    Unused306 = 15,
    TemporaryRedirect = 16,
    BadRequest = 17,
    Unauthorized = 18,
    PaymentRequired = 19,
    Forbidden = 20,
    NotFound = 21,
    MethodNotAllowed = 22,
    NotAcceptable = 23,
    ProxyAuthenticationRequired = 24,
    RequestTimeout = 25,
    Conflict = 26,
    Gone = 27,
    LengthRequired = 28,
    PreconditionFailed = 29,
    RequestEntityTooLarge = 30,
    RequestUrlTooLong = 31,
    UnsupportedMediaType = 32,
    RequestRangeNotSatisfiable = 33,
    ExpectationFailed = 34,
    InternalServerError = 35,
    NotImplemented = 36,
    BadGateway = 37,
    ServiceUnavailable = 38,
    GatewayTimeout = 39,
    HTTPVersionNotSupported = 40
};

typedef struct THttpHeader
{
    char *name;
    char *value;
} THTTPHEADER;

typedef struct THttpRequestPacket
{
    enum HttpMethods method;
    char *resource;
    double httpVersion;
    int headerCount;
    THTTPHEADER **headers;
    int bodySize;
    char *body;
} THTTPREQUESTPACKET;

typedef struct THttpStatus
{
    int code;
    char *text;
} THTTPSTATUS;

typedef struct THttpResponsePacket
{
    THTTPSTATUS status;
    double httpVersion;
    int headerCount;
    THTTPHEADER **headers;
    int bodySize;
    char *body;
} THTTPRESPONSEPACKET;

extern char *HttpMethodsStrings[];
extern THTTPSTATUS HttpStatusValues[];

// Methods start
THTTPRESPONSEPACKET deserializeRequest(char *data, THTTPREQUESTPACKET *deserialized);
enum HttpMethods getHttpMethodFromString(char *methodString);

void addHeader(THTTPHEADER * **headers, int *count, const char* name, const char* value);
int readHeader(char *buff, int len, THTTPHEADER *h);
int getHeadersLen(THTTPHEADER **headers, int count);
void printHeaders(char *buffer, THTTPHEADER **headers, int count);

//void changeStatus();

char *serializeResponse(THTTPRESPONSEPACKET p, int *size);
void writeDefaultResponseHeaders(THTTPRESPONSEPACKET *packet);

void freeReqPacket(THTTPREQUESTPACKET *packet);
void freeResPacket(THTTPRESPONSEPACKET *packet);

#endif