#ifndef FILEFINDER_H_INCLUDED
#define FILEFINDER_H_INCLUDED

#include <stdio.h>

FILE *getFileByResourceName(const char *resource, char **path);
char *getHttpFileTypeString(const char *path);

#endif