#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED

typedef struct {
    int socketId;
} TCONNECTIONDATA;

void startServer();
void stopServer();
void serverCleanupProc(void *arg);
void *serverStartProc(void *vargp);
void *connectionHandler(void *vargp);

#endif